package dao;

public class MainRepository {
	public static PostDAO postDao;
	public static UserDAO userDao;
	public static CommentDAO commentDao;
	public static DirectMessageDAO DMDao;	
	public static FriendRequestDAO friendRequestDAO;
	
	private static MainRepository instance = null;
	
	private MainRepository() {
		userDao = new UserDAO();
		postDao = new PostDAO();
		commentDao = new CommentDAO();
		DMDao = new DirectMessageDAO();	
		friendRequestDAO = new FriendRequestDAO();	
	}
	
	public static MainRepository getInstance() {
		if(instance == null) {
			instance = new MainRepository();
		}
		return instance;
	}
	
	
	
}
