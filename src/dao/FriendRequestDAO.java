package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.DirectMessage;
import beans.FriendRequest;
import beans.Post;
import beans.FriendRequest.FRStatus;
import beans.User;

public class FriendRequestDAO {

	List<FriendRequest> requests;

	public FriendRequestDAO() {
		requests = new ArrayList<FriendRequest>();
		load();
	}

	public List<FriendRequest> getRequests() {
		return requests;
	}

	public void load() {

		File file = new File("database/FriendRequest.csv");
		if (file.exists()) {

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				br.readLine();
				while ((line = br.readLine()) != null) {
					String data[] = line.split(",");
					FriendRequest request = getRequestFromData(data);
					requests.add(request);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("The file does not exist.");
		}

		for (User user : MainRepository.userDao.users) {
			List<FriendRequest> friendRequests = loadRequestsForUser(user.getUsername());
			List<User> friends = loadFriendsForUser(user.getUsername());
			user.setFriendRequests(friendRequests);
			user.setFriends(friends);
		}
	}

	private FriendRequest getRequestFromData(String[] data) {
		User sender = MainRepository.userDao.getUserById(data[0]);
		User reciever = MainRepository.userDao.getUserById(data[1]);
		FRStatus status = FriendRequest.FRStatus.valueOf(data[2]);

		String partsOfTime[] = data[3].split(" ")[1].split(":");
		String partsOfDate[] = data[3].split(" ")[0].split("\\.");

		LocalDateTime date = LocalDateTime.of(Integer.parseInt(partsOfDate[2]), Integer.parseInt(partsOfDate[1]),
				Integer.parseInt(partsOfDate[0]), Integer.parseInt(partsOfTime[0]), Integer.parseInt(partsOfTime[1]));

		FriendRequest request = new FriendRequest(sender, reciever, status, date);
		return request;
	}

	public List<FriendRequest> loadRequestsForUser(String username) {
		List<FriendRequest> userRequests = new ArrayList<FriendRequest>();
		for (FriendRequest fr : requests) {
			if (fr.getReciever().getUsername().equals(username) || fr.getSender().getUsername().equals(username)) {
				userRequests.add(fr);
			}
		}
		return userRequests;
	}

	public List<User> loadFriendsForUser(String username) {
		List<User> userRequests = new ArrayList<User>();
		for (FriendRequest fr : requests) {
			if ((fr.getReciever().getUsername().equals(username))
					&& fr.getStatus().equals(FriendRequest.FRStatus.ACCEPTED)) {
				userRequests.add(fr.getSender());
			} else if ((fr.getSender().getUsername().equals(username))
					&& fr.getStatus().equals(FriendRequest.FRStatus.ACCEPTED)) {
				userRequests.add(fr.getReciever());
			}
		}
		return userRequests;
	}

}
