package dao;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import beans.FriendRequest;
import beans.Post;
import beans.User;

public class UserDAO {

	List<User> users;
	
	public UserDAO() {
		users = new ArrayList<User>();
		load();
	}
	
	
	public void load() {

	    File file = new File("database/user.csv");
	    if (file.exists()) {
	    	
	    	try (BufferedReader br = new BufferedReader(new FileReader(file))) {
	    	    String line;
	    	    br.readLine();
	    	    while ((line = br.readLine()) != null) {
	    	    	String data[] = line.split(",");
	    	    	User u = getUserFromData(data);
	    	    	users.add(u);
	    	    }
	    	} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    	else {
	      System.out.println("The file does not exist.");
	    }
	}
	

	public List<User> getUsers() {
		return users;
	}


	public void setUsers(List<User> users) {
		this.users = users;
	}


	public User getUserById(String id) {
		for(User user : users) {
			if (user.getUsername().equals(id)) {
				return user;
			}
		}
		return null;
	}
	
	public User getUserFromData(String[] data) {
		String username = data[0];
    	String password = data[1];
    	String eMail = data[2];
    	String name = data[3];
    	String surname = data[4];
    	String partsOfDate[] = data[5].split("\\.");
    	LocalDate dateOfBirth  = LocalDate.of(Integer.parseInt(partsOfDate[2]),Integer.parseInt(partsOfDate[1]),Integer.parseInt(partsOfDate[0])) ;
    	User.Gender gender = User.Gender.valueOf(data[6]);
    	User.Role role = User.Role.valueOf(data[7]);
    	BufferedImage profilePicture = null; //todo
    	List<Post> posts = new ArrayList<Post>();
    	List<BufferedImage> pictures = new ArrayList<BufferedImage>(); //todo
    	List<FriendRequest> friendRequests = new ArrayList<FriendRequest>();
    	List<User> friends = new ArrayList<User>();
    	Boolean privateAccount = Boolean.parseBoolean(data[10]);
    	User user = new User(username, password, eMail, name, surname, dateOfBirth,
		gender,  role,  profilePicture, posts, pictures,
		friendRequests,  friends,  privateAccount);
    	return user;
	}
}
