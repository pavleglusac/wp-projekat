package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.DirectMessage;
import beans.Post;
import beans.User;

public class DirectMessageDAO {

	List<DirectMessage> dms;

	public DirectMessageDAO() {
		dms = new ArrayList<DirectMessage>();
		load();
	}

	public List<DirectMessage> getDms() {
		return dms;
	}

	public void setDms(List<DirectMessage> dms) {
		this.dms = dms;
	}

	public void load() {

		File file = new File("database/DirectMessage.csv");
		if (file.exists()) {

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				br.readLine();
				while ((line = br.readLine()) != null) {
					String data[] = line.split(",");
					DirectMessage dm = getDMFromData(data);
					dms.add(dm);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("The file does not exist.");
		}
	}

	private DirectMessage getDMFromData(String[] data) {
		User sender = MainRepository.userDao.getUserById(data[0]);
		User reciever = MainRepository.userDao.getUserById(data[1]);
		String content = data[2];
		
		String[] timeRead = data[3].split(" ")[1].split(":");
		String[] dateRead = data[3].split(" ")[0].split("\\.");
		
		LocalDateTime date = LocalDateTime.of(Integer.parseInt(dateRead[2]), Integer.parseInt(dateRead[1]),
				Integer.parseInt(dateRead[0]), Integer.parseInt(timeRead[0]), Integer.parseInt(timeRead[1]));

		DirectMessage dm = new DirectMessage(sender, reciever, content, date);
		return dm;
	}

}
