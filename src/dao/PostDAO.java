package dao;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.Post;
import beans.User;

public class PostDAO {

	List<Post> posts;

	public PostDAO() {
		posts = new ArrayList<Post>();
		load();
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public void load() {

		File file = new File("database/Post.csv");
		if (file.exists()) {

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				br.readLine();
				while ((line = br.readLine()) != null) {
					String data[] = line.split(",");
					System.out.println(line);
					Post post = getPostFromData(data);
					posts.add(post);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("The file does not exist.");
		}

		for (User user : MainRepository.userDao.users) {
			List<Post> posts = loadPostsForUser(user.getUsername());
			user.setPosts(posts);
		}
	}

	private Post getPostFromData(String[] data) {
		String postId = data[0];
		User user = MainRepository.userDao.getUserById(data[1]);
		BufferedImage picture = null; // data[2]
		String text = data[3];
		List<Comment> comments = new ArrayList<Comment>();
		
		String partsOfTime[] = data[4].split(" ")[1].split(":");
		String partsOfDate[] = data[4].split(" ")[0].split("\\.");

		LocalDateTime date = LocalDateTime.of(Integer.parseInt(partsOfDate[2]), Integer.parseInt(partsOfDate[1]),
				Integer.parseInt(partsOfDate[0]), Integer.parseInt(partsOfTime[0]), Integer.parseInt(partsOfTime[1]));
		
		Post post = new Post(postId, user, picture, text, comments, date);
		return post;
	}

	public List<Post> loadPostsForUser(String username) {
		List<Post> userPosts = new ArrayList<Post>();
		for (Post post : posts) {
			if (post.getUser().getUsername().equals(username)) {
				userPosts.add(post);
			}
		}
		return userPosts;
	}

	public Post getPostById(String id) {
		for (Post post : posts) {
			if (post.getPostId().equals(id)) {
				return post;
			}
		}
		return null;
	}

}
