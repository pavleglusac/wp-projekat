package dao;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import beans.Post;
import beans.User;

public class CommentDAO {

	List<Comment> comments;

	public CommentDAO() {
		comments = new ArrayList<Comment>();
		load();
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public void load() {

		File file = new File("database/Comment.csv");
		if (file.exists()) {

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				br.readLine();
				while ((line = br.readLine()) != null) {
					String data[] = line.split(",");
					Comment comment = getCommentFromData(data);
					comments.add(comment);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("The file does not exist.");
		}

		for (Post post : MainRepository.postDao.posts) {
			List<Comment> comments = loadCommentsForPost(post.getPostId());
			post.setComments(comments);
		}
	}

	private Comment getCommentFromData(String[] data) {
		Post post = MainRepository.postDao.getPostById(data[0]);
		User user = post.getUser();

		System.out.println(data[1]);
		String[] timeRead = data[1].split(" ")[1].split(":");
		String[] dateRead = data[1].split(" ")[0].split("\\.");
		
		String partsOfDate[] = data[1].split("\\.");
		LocalDateTime date = LocalDateTime.of(Integer.parseInt(dateRead[2]), Integer.parseInt(dateRead[1]),
				Integer.parseInt(dateRead[0]), Integer.parseInt(timeRead[0]), Integer.parseInt(timeRead[1]));

		LocalDateTime dateOfEdit = null;
		if (data.length > 2) {
			
			String[] editTimeRead = data[2].split(" ")[1].split(":");
			String[] editDateRead = data[2].split(" ")[0].split("\\.");
			
			dateOfEdit = LocalDateTime.of(Integer.parseInt(editDateRead[2]), Integer.parseInt(editDateRead[1]),
					Integer.parseInt(editDateRead[0]), Integer.parseInt(editTimeRead[0]), Integer.parseInt(editTimeRead[1]));
		}
		Comment comment = new Comment(user, post, date, dateOfEdit);
		return comment;
	}

	public List<Comment> loadCommentsForPost(String postId) {
		List<Comment> postComments = new ArrayList<Comment>();
		for (Comment comment : comments) {
			if (comment.getPost().getPostId().equals(postId)) {
				postComments.add(comment);
			}
		}
		return postComments;
	}

}
