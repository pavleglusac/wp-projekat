package rest;

import static spark.Spark.get;
import static spark.Spark.path;

public class Registration {
	
	private static final String basePath = "/registration";
	
	public Registration() {
		
	}
	
	public static void init() {
		
		path(basePath, () -> {
			
			get("", (req, res) -> {
				return "registration";
			});
			
		});
		
		
	}
	
}
