package rest;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.path;
import static spark.Spark.staticFiles;
import static spark.Spark.webSocket;


public class Login {
	
	private static final String basePath = "/login";
	
	public Login() {
		
	}
	
	public static void init() {
		
		path(basePath, () -> {
			
			get("", (req, res) -> {
				return "login";
			});
			
		});
		
		
	}
	
	
}
