package rest;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.staticFiles;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import dao.MainRepository;

public class Main {

	private static Map<String, Object> root;
	
	public static void main(String[] args) throws IOException {
		port(8080);
		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		Login.init();
		Registration.init();
		
		MainRepository mainRepository = MainRepository.getInstance();
		
		get("/test", (req, res) -> {
			return "Works";
		});
	}

}
