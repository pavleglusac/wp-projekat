package beans;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.util.List;



public class User {
	public enum Gender {
		  MALE,
		  FEMALE
		}
	
	
	public enum Role {
		  ADMIN,
		  USER
		}

	private String username;
	private String password;
	private String eMail;
	private String name;
	private String surname;
	private LocalDate dateOfBirth;
	private Gender gender;
	private Role role;
	private BufferedImage profilePicture;
	private List<Post> posts;
	private List<BufferedImage> pictures;
	private List<FriendRequest> friendRequests;
	private List<User> friends;
	private Boolean privateAccount;
	
	
	
	public User() {
	}

	

	public User(String username, String password, String eMail, String name, String surname, LocalDate dateOfBirth,
			Gender gender, Role role, BufferedImage profilePicture, List<Post> posts, List<BufferedImage> pictures,
			List<FriendRequest> friendRequests, List<User> friends, Boolean privateAccount) {
		super();
		this.username = username;
		this.password = password;
		this.eMail = eMail;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.role = role;
		this.profilePicture = profilePicture;
		this.posts = posts;
		this.pictures = pictures;
		this.friendRequests = friendRequests;
		this.friends = friends;
		this.privateAccount = privateAccount;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}


	public List<Post> getPosts() {
		return posts;
	}
	

	public void addPost(Post post) {
		posts.add(post);
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<FriendRequest> getFriendRequests() {
		return friendRequests;
	}
	

	public void addFriendRequest(FriendRequest friendRequest) {
		friendRequests.add(friendRequest);
	}

	public void setFriendRequests(List<FriendRequest> friendRequests) {
		this.friendRequests = friendRequests;
	}

	public List<User> getFriends() {
		return friends;
	}	

	public void addFriend(User friend) {
		friends.add(friend);
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}

	public Boolean getPrivateAccount() {
		return privateAccount;
	}

	public void setPrivateAccount(Boolean privateAccount) {
		this.privateAccount = privateAccount;
	}

	public BufferedImage getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(BufferedImage profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<BufferedImage> getPictures() {
		return pictures;
	}	

	public void addPicture(BufferedImage picture) {
		pictures.add(picture);
	}

	public void setPictures(List<BufferedImage> pictures) {
		this.pictures = pictures;
	}
	
	
	
	 
}
