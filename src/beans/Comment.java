package beans;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Comment {
	private User user;
	private Post post;
	private LocalDateTime date;
	private LocalDateTime dateOfEdit;
	
	public Comment(User user,Post post, LocalDateTime date, LocalDateTime dateOfEdit) {
		this.user = user;
		this.post = post;
		this.date = date;
		this.dateOfEdit = dateOfEdit;
	}
	
	

	public Post getPost() {
		return post;
	}



	public void setPost(Post post) {
		this.post = post;
	}



	public Comment() {
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public LocalDateTime getDateOfEdit() {
		return dateOfEdit;
	}

	public void setDateOfEdit(LocalDateTime dateOfEdit) {
		this.dateOfEdit = dateOfEdit;
	}

	
}
