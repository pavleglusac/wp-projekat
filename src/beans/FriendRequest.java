package beans;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class FriendRequest {
	public enum FRStatus{
		ACCEPTED,
		ONHOLD,
		REJECTED
	}
	
	public User sender;
	public User reciever;
	public FRStatus status;
	public LocalDateTime date;
	
	public FriendRequest(User sender, User reciever, FRStatus status, LocalDateTime date) {
		this.sender = sender;
		this.reciever = reciever;
		this.status = status;
		this.date = date;
	}

	public FriendRequest() {
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReciever() {
		return reciever;
	}

	public void setReciever(User reciever) {
		this.reciever = reciever;
	}

	public FRStatus getStatus() {
		return status;
	}

	public void setStatus(FRStatus status) {
		this.status = status;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	
	

}
