package beans;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.util.List;

public class Post {
	private String postId;
	private User user;
	private BufferedImage picture;
	private String text;
	private List<Comment> comments;
	private LocalDateTime date;
	
	
	public Post(String postId, User user, BufferedImage picture, String text, List<Comment> comments, LocalDateTime date) {
		this.postId = postId;
		this.user = user;
		this.picture = picture;
		this.text = text;
		this.comments = comments;
		this.date = date;
	}

	public Post() {
	}
	
	
	
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public BufferedImage getPicture() {
		return picture;
	}
	
	public void setPicture(BufferedImage picture) {
		this.picture = picture;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void addComment(Comment comment) {
		comments.add(comment);
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}	
	
	
	
}
			