package beans;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DirectMessage {
	public User sender;
	public User reciever;
	public String content;
	public LocalDateTime date;
	
	public DirectMessage(User sender, User reciever, String content, LocalDateTime date) {
		this.sender = sender;
		this.reciever = reciever;
		this.content = content;
		this.date = date;
	}
	
	public DirectMessage() {
		
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReciever() {
		return reciever;
	}

	public void setReciever(User reciever) {
		this.reciever = reciever;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	
}
