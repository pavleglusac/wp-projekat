Vue.component('message-list-item', 
    {
        props: ['message'],
        template:`
            <li>
                <a class="dropdown-item" href="#"> 
                    <strong> {{message.sender}} </strong>: {{message.content}}
                </a>
                <hr class="dropdown-divider"></hr>
            </li>
        `,
        created: function() {
            console.log(this.message);
        }
    }
);