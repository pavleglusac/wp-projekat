Vue.component('message-list',
    {
        props: ['messages'],
        template: `
        <div class="row fixed-bottom">
            <div class=" p-0 col-9"></div>
            <div class="btn-group dropup col-3 p-0">
                <button type="button" id="dropup" class="btn bg-dark text-white dropdown-toggle" data-bs-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                Messages
                </button>
                <ol class="dropdown-menu p-0" aria-labelledby="dropup" >
                    <message-list-item v-for="item in messages" v-bind:message="item"></message-list-item>
                    
                    <li><a class="dropdown-item">Hello</a></li>
                    <li><hr class="dropdown-divider"></hr></li>

                </ol>
            </div>
        
        </div>
        `,
        mounted: function() {
            console.log(this.messages);
        }
    }

)